<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
//use Kyslik\ColumnSortable\Sortable;

class Adminusers extends Authenticatable
{
    use Notifiable;
   // use Sortable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname',
        'email',
        'password',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
     //public $sortable = ['uid', 'email', 'good_form_shots','username','bad_form_shots'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
            ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    
}
