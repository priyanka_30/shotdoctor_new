<?php

namespace App\Http\Controllers;

//require_once '../../vendor/autoload.php';

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class FirebaseController extends Controller
{
    //
     public function index() {

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://shotdoctor-43cdd.firebaseio.com/')
        ->create();

         
        $database   =   $firebase->getDatabase();
        $createPost    =   $database
        ->getReference('blog/posts')
        ->push([
            'title' =>  'shotdoctor setup ',
            'body'  =>  'ready to start shotdoctor setup.'

        ]);
            $database   =   $firebase->getDatabase()->getReference('Authentication');
        echo '<pre>';
        print_r($database);
         echo '</pre>';die();
    }

    public function signup(Request $request) {

     	 $data = $request->input();
     	//echo "<pre>"; print_r($data); echo "</pre>"; 

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://shotdoctor-43cdd.firebaseio.com/')
        ->create();

        $database   =   $firebase->getDatabase();
        $createPost    =   $database
        ->getReference('signup/data')
        ->push([
            'fullname' =>  $data['fullname'],
            'email'  =>  $data['email'],
            'password'  =>  $data['password'],
            'passwd_confirm'  =>  $data['passwd_confirm'],

        ]);
            
        echo '<pre>';
        print_r($createPost->getvalue());
        echo '</pre>';
    }

}
