<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Adminusers;
use Auth;

    use Session;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function main()
    {
        $this->setsession('');
        return view('main');
    }

    public function signup(){
        $this->setsession('');
        return view('signup');
    }

    public function signin(){
        $this->setsession('');
        $error ="";
         return View('signin', [
                'error' => $error,

            ]);
    }

    public function setsession($pageopen){
        Session::put('key', $pageopen);
    }
    public function checksession(){
        $value = Session::get('key');
        if($value!='pageopen'){
            $this->signin();
        }
    }
    public function dashboard(){
        $this->setsession('pageopen');
        return redirect('/userdata');
        return view('dashboard');
    }

    public function users(){
        $value = session()->get('email');
        if($value!=""){
           return view('users');
        }else{
            
             return redirect('/signin');
        }
    }

    public function totalshots(){
        $value = session()->get('email');
      
        if($value!=""){

            return view('totalshots');
                
        }else{

           return redirect('/signin');
        }

    }
    
    public function bktotalshots(){
        $value = Session::get('key');
        if($value!='pageopen'){
                return redirect('/signin');
        }else{
            return view('bktotalshots');
        }
    }

    public function overallshooting(){
        $value = session()->get('email');
        if($value!=""){
            return view('overallshooting');
               
        }else{
            return redirect('/signin');
        }
    }

    public function forgotpassword(){
        $this->setsession('');
        return view('forgotpassword');
    }

    public function verifyemail(){
        $this->setsession('');
        return view('verifyemail');
    }

    public function verifysuccess(){
        $this->setsession('');
        return view('verifysuccess');
    }

    public function table(){
        return view('table');
    }

    public function registersignup(Request $request)
    {
        $this->layout = null;
        $data = $request->all();
        // echo "<pre>"; print_r($data); die();
        if(isset($data['fullname']) && $data['fullname']!=""){ $fullname =  $data['fullname']; } else { $fullname ='';}
        
        if(isset($data['email']) && $data['email']!=""){ $email =  $data['email']; } else { $email = '';}
        if(isset($data['password']) && $data['password']!=""){ $password =  $data['password']; } else { $password = '';}
        $existuser = Adminusers::where('email', '=', $email)->first();
        if($existuser){
            echo "0"; die();
        }
        else{
        $user = Adminusers::create([
                'fullname' => $fullname,
                'email' => $email,
                'password' => base64_encode($password),
            ]);
            echo "1"; die();
        }

    }    


    public function userlogin(Request $request)
    {
         $data = $request->input();
         $error ="";
        
            if(isset($data['email']) && $data['email']!="" && isset($data['password']) && $data['password']!=""){
                $password = base64_encode($data['password']);
               
                $existuser = Adminusers::where('email', '=', $data['email'])->where('password', $password)->first();
                
                if(!empty($existuser)) {
                   session()->put('email', $data['email']);
                   return redirect('/dashboard');

                }
                else{
                    //session()->put('error', "Incorrect User Details");
                   $error = " Incorrect Details";
                    //return view('/signin');
                    return View('/signin', [
                'error' => $error,

            ]);
                }
            }
        
    }

    public function userlogout()
    {
        Session::forget('email');
        return redirect('/signin');
    }


    
    
}
