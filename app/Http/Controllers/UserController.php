<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\User;
use App\Drill;
    
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($col=null,$type=null)
    {
       $value = Session::get('key');
       if($value!='pageopen'){
               return redirect('/signin');
       }else{
           
        $sortone = 'uid'.'/asc';
        $sorttwo = 'email'.'/asc';
        $sortthree = 'username'.'/asc';
        $sortfour = 'osa'.'/asc';
        $sortfive = 'total_shots'.'/asc';
        $searchtxt = '';
        if($col!=""){
            if($col=='uid'){
                if($type=="desc"){ $sortone = 'uid'.'/asc'; }else { $sortone = 'uid'.'/desc'; }
            }
            if($col=='email'){
                if($type=="desc"){ $sorttwo = 'email'.'/asc'; }else { $sorttwo = 'email'.'/desc'; }
            }
            if($col=='username'){
                if($type=="desc"){ $sortthree = 'username'.'/asc'; }else { $sortthree = 'username'.'/desc'; }
            }
            if($col=='osa'){
                if($type=="desc"){ $sortfour = 'osa'.'/asc'; }else { $sortfour = 'osa'.'/desc'; }
            }
            if($col=='total_shots'){
                if($type=="desc"){ $sortfive = 'total_shots'.'/asc'; }else { $sortfive = 'total_shots'.'/desc'; }
            }
        }else {
            $col = 'total_shots'; $type = 'desc';
        }
        if(isset($_GET['search']) && $_GET['search']!=""){
            $q = $_GET['search'];
            $searchtxt = $q;
            $products = User::orderBy($col,$type)->where('email', 'LIKE', '%'.$q.'%')
        ->orWhere('username', 'LIKE', '%'.$q.'%')
        ->orWhere('uid', 'LIKE', '%'.$q.'%')->paginate(25);
        }else {
            $products = User::orderBy($col,$type)->paginate(25);
        }
        
        return view('user',compact('products','sortone','sorttwo','sortthree','sortfour','sortfive','searchtxt'));
       }
    }
    
    public function detail($id,$col=null,$type=null)
    {
        $value = Session::get('key');
        if($value!='pageopen'){
                return redirect('/signin');
        }else{
            $searchtxt = '';
            $avg = 0;
            $goodtotal = 0;
            $sortone = 'created_on'.'/asc';
            $sorttwo = 'drill_type'.'/asc';
            $sortthree = 'level'.'/asc';
            $sortfour = 'guide_hand_type'.'/asc';
            $sortfive = 'bad_form_shots'.'/asc';
            $sortsix = 'good_form_shots'.'/asc';
            $sortseven = 'total_shots'.'/asc';
            $sorteight = 'good_form_shots'.'/asc';
            if($col!=""){
                if($col=='created_on'){
                    if($type=="desc"){ $sortone = 'created_on'.'/asc'; }else { $sortone = 'created_on'.'/desc'; }
                }
                if($col=='drill_type'){
                    if($type=="desc"){ $sorttwo = 'drill_type'.'/asc'; }else { $sorttwo = 'drill_type'.'/desc'; }
                }
                if($col=='level'){
                    if($type=="desc"){ $sortthree = 'level'.'/asc'; }else { $sortthree = 'level'.'/desc'; }
                }
                if($col=='guide_hand_type'){
                    if($type=="desc"){ $sortfour = 'guide_hand_type'.'/asc'; }else { $sortfour = 'guide_hand_type'.'/desc'; }
                }
                if($col=='bad_form_shots'){
                    if($type=="desc"){ $sortfive = 'bad_form_shots'.'/asc'; }else { $sortfive ='bad_form_shots'.'/desc'; }
                }
                if($col=='good_form_shots'){
                    if($type=="desc"){ $sortsix = 'good_form_shots'.'/asc'; }else { $sortsix ='good_form_shots'.'/desc'; }
                }
                if($col=='total_shots'){
                    if($type=="desc"){ $sortseven = 'total_shots'.'/asc'; }else { $sortseven ='total_shots'.'/desc'; }
                }
                if($col=='good_form_shots'){
                    if($type=="desc"){ $sorteight = 'good_form_shots'.'/asc'; }else { $sorteight ='good_form_shots'.'/desc'; }
                }
            }else {
                $col = 'created_on'; $type = 'desc';
            }
            if(isset($_GET['search']) && $_GET['search']!=""){
               
                $q = $_GET['search'];
                $searchtxt = $q;
                if($q=='Form'){ $drillq = 0; }else if($q=='Range'){ $drillq = 1; }
                else if($q=='Rhythm'){ $drillq = 2; } else { $drillq = $q; }
                
                if($q=='Rookie'){ $levelq = 0; }else if($q=='Collegiate'){ $levelq = 1; }
                else if($q=='Pro'){ $levelq = 2; } else { $levelq = $q; }
                
                if($q=='Right hand' || $q=='Right'){ $handq = 0; }else if($q=='Left hand' || $q=='Left'){ $handq = 1; }
                else { $handq = $q; }
                
                if(substr_count($q, "/")==2){
                    $q = date('Y-m-d',strtotime($q));
                    $products = Drill::where('uid','=',$id)->orderBy($col,$type)
                    ->Where('created_on', 'LIKE', '%'.$q.'%')
                    ->paginate(100);
                }else if($q=='Form' || $q=='form' || $q=='Range' || $q=='range' || $q=='Rhythm' || $q=='rhythm'){
                    $products = Drill::where('uid','=',$id)->orderBy($col,$type)
                    ->Where('drill_type', '=', $drillq)
                    ->paginate(100);
                }else if($q=='Rookie' || $q=='rookie' || $q=='Collegiate' || $q=='collegiate' || $q=='Pro' || $q=='pro'){
                    $products = Drill::where('uid','=',$id)->orderBy($col,$type)
                    ->Where('level', '=', $levelq)
                    ->paginate(100);
                }else if($q=='Right hand' || $q=='Left hand' || $q=='Right' || $q=='Left' || $q=='left' || $q=='right'){
                    $products = Drill::where('uid','=',$id)->orderBy($col,$type)
                    ->Where('guide_hand_type', '=', $handq)
                    ->paginate(100);
                }else {
                    if(is_numeric($q)){
                        $products = Drill::orderBy($col,$type)
                        ->orwhere([['bad_form_shots', '=', $q],['uid','=',$id]])
                        ->orwhere([['good_form_shots', '=', $q],['uid','=',$id]])
                        ->orwhere([['total_shots', '=', $q],['uid','=',$id]])
                        ->where('uid','=',$id)
                        ->paginate(100);
                    }else {
                        $products = Drill::orderBy($col,$type)
                        ->where('uid', '=', $q)
                        ->paginate(100);
                    }
                }
                
            }else {
                $products = Drill::where('uid','=',$id)->orderBy($col,$type)->paginate(100);
                
            }
            $total = 0;
            if($total>0){
                $goodtotal = Drill::where('uid','=',$id)->sum('good_form_shots');//echo "<br>";
                $avg = round(($goodtotal/$total)*100);
            }
            $drilltype = array('Form','Range','Rhythm');
            $leveltype = array('Rookie','Collegiate','Pro');
            $handtype = array('Right hand','Left hand');
            return view('detail',compact('products','avg','goodtotal','drilltype','leveltype','handtype','id','searchtxt','sortone','sorttwo','sortthree','sortfour','sortfive','sortsix','sortseven','sorteight',));
        }
    } 
    
    public function updatedata(){
        $col = 'total_shots'; $type = 'desc';
        $products = User::orderBy($col,$type)->select('uid','id')->get();
        //updatedata
       // echo "<pre>";print_r($products);
        foreach($products as $productlist){
            $total = Drill::where('uid','=',$productlist['uid'])->sum('total_shots');//echo "<br>";
            if($total>0){
                $goodtotal = Drill::where('uid','=',$productlist['uid'])->sum('good_form_shots');//echo "<br>";
                $avg = round(($goodtotal/$total)*100);//echo "<br>";
                $productlist['id'];//echo "<br>";
                $userupdate = User::where('id','=',$productlist['id'])->update(['total_shots' => $total,'osa'=>$avg]);
            }
        }
        echo "success"; die();
    }

    
}
