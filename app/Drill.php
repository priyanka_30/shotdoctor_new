<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
//use Kyslik\ColumnSortable\Sortable;

class Drill extends Authenticatable
{
    use Notifiable;
   // use Sortable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid', 'bad_form_shots', 'good_form_shots','drill_type','total_shots'
    ];
     //public $sortable = ['uid', 'email', 'good_form_shots','username','bad_form_shots'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
            ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    
}
