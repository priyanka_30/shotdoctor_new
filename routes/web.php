<?php

// Route::redirect('/admin', '/login');
//Route::redirect('/', '/index.php/main');

// Route::redirect('/home', '/admin');

// Auth::routes(['register' => false]);

 Route::get('/', 'HomeController@main')->name('main');

Route::get('/signup', 'HomeController@signup')->name('signup'); 

Route::get('/signin', 'HomeController@signin')->name('signin'); 

Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard'); 

Route::get('/users', 'HomeController@users')->name('users'); 

Route::get('/totalshots', 'HomeController@totalshots')->name('totalshots'); 

Route::get('/bktotalshots', 'HomeController@bktotalshots')->name('bktotalshots'); 

    
Route::get('/overallshooting', 'HomeController@overallshooting')->name('overallshooting'); 

Route::get('/forgotpassword', 'HomeController@forgotpassword')->name('forgotpassword'); 

Route::get('/verifyemail', 'HomeController@main')->name('main'); 

Route::get('/table', 'HomeController@table')->name('table'); 

Route::get('/userdata', 'UserController@index')->name('index');
Route::get('/userdata/{col}/{type}', 'UserController@index')->name('index');

Route::get('/detail/{id}', 'UserController@detail')->name('detail');
Route::get('/detail/{id}/{col}/{type}', 'UserController@detail')->name('detail');

Route::get('/updatedata', 'UserController@updatedata')->name('updatedata');

Route::post('/registersignup', 'HomeController@registersignup')->name('registersignup');

Route::post('/userlogin', 'HomeController@userlogin')->name('userlogin');

Route::get('/userlogout', 'HomeController@userlogout')->name('userlogout'); 



// Route::post('/apilogin', 'HomeController@apilogin');
// Route::post('/apiregister', 'HomeController@apiregister');
// Route::post('/apiforgot', 'HomeController@apiforgot');

// Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
//     Route::get('/', 'HomeController@index')->name('home');

//     Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

//     Route::resource('permissions', 'PermissionsController');

//     Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

//     Route::resource('roles', 'RolesController');

//     Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

//     Route::resource('users', 'UsersController');

//     Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');

//     Route::resource('products', 'ProductsController');
// });
