@extends('layouts.shotdoctor')
@section('content')
    <div style="margin-bottom: 40px;margin-top: -50px;margin-left: 5px;" class="row">
        <div class="col-lg-12">
            <a class="dashboard-a" href="#">
                Metrics / Overall Shooting Accuracy
            </a>
        </div>
    </div>
<div>
    
    <div>
        <div class="table-responsive">
             <table id="example" class="display" width="100%"></table>

        </div>
    </div>
</div>
<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-firestore.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>


    <script>
      // Your web app's Firebase configuration
      var firebaseConfig = {
        apiKey: "AIzaSyDyhC-L2doy8eKRcqlQEsPO3c2YdsbSf_w",
        authDomain: "shotdoctor80.firebaseapp.com",
        databaseURL: "https://shotdoctor80.firebaseio.com",
        projectId: "shotdoctor80",
        storageBucket: "shotdoctor80.appspot.com",
        messagingSenderId: "623442148061",
        appId: "1:623442148061:web:77b2a10f4d21e14ebfb934",
        measurementId: "G-3FB0FR9PR1"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);

      const auth = firebase.auth();
      const db = firebase.firestore();
      //firebase.analytics();
    </script>
    <!-- <script type="text/javascript" src="/js/auth.js"> </script> -->
    <!-- <script type="text/javascript" src="/js/login.js"> </script> -->
    <script type="text/javascript">

       var a =  window.location.href;
     
      if(a == "http://metrics.shot.doctor/index.php/overallshooting"){
    firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    var displayName = user.displayName;
    var email = user.email;
    var uid = user.uid;
    var providerData = user.providerData;
   
    document.getElementById("profile-name").innerHTML = displayName;
    var res = displayName.charAt(0);
    document.getElementById("name-first").innerHTML = res;
    console.log("name",displayName);
    console.log(email);
    
    
  } else {
    // location.replace("/index.php/signin");
  }
});
}
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        var displayName = user.displayName;
        var email = user.email;
        var uid = user.uid;
        var providerData = user.providerData;
        
        document.getElementById("profile-name").innerHTML = displayName;
        var res = displayName.charAt(0);
        document.getElementById("name-first").innerHTML = res;
        //console.log(displayName);
        //console.log(email);
        let serial =1;
        var outterArray = [];
        
            var userRef = firebase.database().ref("users/");
            userRef.on("child_added", function(snapshot) {
            var userdata = snapshot.val();
            //           console.log('userdata',userdata);
            var bad_shots = userdata.bad_form_shots;
            var good_shots = userdata.good_form_shots;
            var total_shots = bad_shots + good_shots;
            var email = null;
            if(userdata.email){
               email = userdata.email; 
            }

            var shot_accuracy = parseInt(total_shots/good_shots) *100;
            if(isNaN(shot_accuracy)){
                shot_accuracy=0;
            }
            if(userdata.username && email){
                var innerArray = [serial,userdata.username, email, shot_accuracy,'' ];
                outterArray.push(innerArray);
                serial=parseInt(serial)+1;
            }
          
      });
                                       
                                  
                                       
        setTimeout(function(){
            console.log("outterArraysss--",outterArray);
            $('#example').DataTable( {
             "pageLength": 25,
             data: outterArray,
             columns: [
             { title: "Id" },
             { title: "Username" },
             { title: "Email" },
             { title: "Accuracy" },
             { title: "" },
             ]
             } );         
        },5000);
                                       
        setTimeout(function(){ $(".form-control-sm").attr("placeholder", "Enter your search details"); },6000);
        
        
      } else {
        // No user is signed in.
        
      }
    });
</script>
@endsection


