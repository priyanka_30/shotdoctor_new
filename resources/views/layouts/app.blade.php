<?php 
        header('Cache-Control: no-store, private, no-cache, must-revalidate');                  // HTTP/1.1
        header('Cache-Control: pre-check=0, post-check=0, max-age=0, max-stale = 0', false);    // HTTP/1.1
        header('Pragma: public');
        header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');                                       // Date in the past  
        header('Expires: 0', false); 
        header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
        header('Pragma: no-cache');
  ?> 
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ trans('global.site_title') }}</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/css/adminltev3.css" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet" />
</head>

<body class="header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden login-page">
<nav class="navbar navbar-expand-lg fixed-top">
  <div class="container">
    <a class="navbar-brand" href="/"><img src="{{ ('/images/logo.png') }}"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link main-a" href="https://www.shot.doctor/" target="_blank">HOME </a>
        </li>
       
        <li class="nav-item">
          <a class="nav-link main-a" href="https://www.shot.doctor/#about-us" target="_blank">ABOUT</a>
        </li>
        <li class="nav-item">
          <a class="nav-link main-a" href="https://www.shot.doctor/contact" target="_blank">CONTACT</a>
        </li>
        <li class="nav-item">
          <a class="nav-link main-a" href="https://www.shot.doctor/blog" target="_blank">BLOG</a>
        </li>
        <li class="nav-item">
          <a class="nav-link main-a" href="https://apps.apple.com/us/app/shotdoctor/id1225746385" target="_blank">DOWNLOAD</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  {{ Route::currentRouteName()=='signup' ? 'active-p' : 'main-a' }}" href="/index.php/signup">SIGNUP</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ Route::currentRouteName()=='signin' ? 'active-p' : 'main-a' }}" href="/index.php/signin">LOGIN</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
    @yield('content')
</body>

</html>
