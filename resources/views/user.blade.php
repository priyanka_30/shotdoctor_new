@extends('layouts.shotdoctor')
@section('content')
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

    
<div>
    
    <div>
       
        <div style="width:500px; margin-bottom:20px;">
        <div style="float:left;margin-right:10px;margin-bottom:20px;padding-top:5px;">Search:</div>
           <form action="/index.php/userdata/" method="get">
            <div style="float:left;margin-right:5px;margin-bottom:20px;"><input type="search" name="search" value="{{$searchtxt}}" class="form-control" placeholder="Enter your search details" style="border-radius:20px;padding-left:15px; width:250px;float:left;">
           <input type="submit" value="GO" class="btn btn-default login-btn login" style="float: left;width: 90px;margin-top: 0px;margin-left: 10px;">
            </div>
            </form>
        </div>
        <div class="table-responsive">    
            <table class="table table-bordered">
                <tr>
                    <th>User<a href="/index.php/userdata/{{ $sortthree }}"><i class="fa fa-fw fa-sort"></i></a></th>
                    <th>Email <a href="/index.php/userdata/{{ $sorttwo }}"><i class="fa fa-fw fa-sort"></i></a></th>
                    
                    
                    <th>Total Shot <a href="/index.php/userdata/{{ $sortfive }}"><i class="fa fa-fw fa-sort"></i></a></th>
                    <th>OSA  <a href="/index.php/userdata/{{ $sortfour }}"><i class="fa fa-fw fa-sort"></i></a></th>
                    <th>Action</th>
                </tr>
                @if($products->count())
                    @foreach($products as $key => $product)
                        <tr>
                            <td>{{ $product->username }}</td>
                            <td>{{ $product->email }}</td>
                            <td>{{ $product->total_shots }}</td>
                            @if($product->osa>0)
                            <td>{{ $product->osa }}%</td>
                            @else
                            <td>{{ $product->osa }}</td>
                            @endif
                            <td>
                                <a href="/index.php/detail/{{ $product->uid }}">View</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                  <tr>
                      <td colspan="6" align="center">No record found</td>
                      
                  </tr>
                @endif
            </table>
    {!! $products->appends(\Request::except('page'))->render() !!}
</div>
    </div>
</div>
@endsection
