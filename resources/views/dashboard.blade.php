@extends('layouts.shotdoctor')
@section('content')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="dashboard-a" href="#">
                Dashboard/
            </a>

        </div>
    </div>

@endsection

<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-firestore.js"></script>

    <script>
      // Your web app's Firebase configuration
      var firebaseConfig = {
        apiKey: "AIzaSyDyhC-L2doy8eKRcqlQEsPO3c2YdsbSf_w",
        authDomain: "shotdoctor80.firebaseapp.com",
        databaseURL: "https://shotdoctor80.firebaseio.com",
        projectId: "shotdoctor80",
        storageBucket: "shotdoctor80.appspot.com",
        messagingSenderId: "623442148061",
        appId: "1:623442148061:web:77b2a10f4d21e14ebfb934",
        measurementId: "G-3FB0FR9PR1"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);

      const auth = firebase.auth();
      const db = firebase.firestore();
      //firebase.analytics();
    </script>
    <script type="text/javascript" src="/js/auth.js"> </script>
    <script type="text/javascript" src="/js/login.js"> </script>