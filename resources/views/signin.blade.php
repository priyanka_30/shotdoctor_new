@extends('layouts.app')
<!-- Navigation -->
<link href="{{ ('/css/custom.css') }}" rel="stylesheet">
<style type="text/css">
  .error{
    color: red;
    justify-content: left !important;
    padding-left: 14px;
    font-weight: normal !important;
    font-size: 14px;
}
</style>
<div class="bg-img">


<!-- Full Page Image Header with Vertically Centered Content -->
<header class="masthead">
  <div class="container h-100">
    <div class="row h-100 align-items-center">
      <div class="col-md-6 text-center">
        <div class="col-md-12">
        <h3 class="font-weight-light main-h3">A PROFESSIONAL BASKETBALL COACH ON YOUR WRIST</h3></div>
        <div class="login-div" id="login_div">
            <p class="login-heading">LOGIN</p>
            <form method="post" action="{{url('userlogin')}}" id="myForm">
               @csrf
              <div class="col-md-12">
               
                <span id="error-email" style=" color: red; margin-bottom: 10px;"> <?php echo $error; ?></span>
                <span id="success-email" style="background: red; color: #000; display: none; margin-bottom: 10px;"></span>
                <div id="result"></div>
              </div>
            <div class="form-horizontal login-m-d" id="login-m-d">
              <div class="control-group form-inline">
                <label for="name" class="col-md-3 login-label"> EMAIL</label>
                <div class="controls col-md-9">
                  <input type="email" id="email_field" onkeyup="validate_email(this);" class="login-input signup_email" name="email" placeholder="name@email.com">
                  <label id="signup-email-error" class="error" for="signup_email" style="display: none;">This field is required.</label>
                </div>
              </div>
            </div>
            <div class="form-horizontal">
              <div class="control-group form-inline">
                <label for="name" class="col-md-3 login-label"> PASSWORD</label>
                <div class="controls col-md-9">
                  <input type="password" id="password_field" onkeyup="validate_password(this);" name="password" class="login-input signup_password" placeholder="*****">
                  <label id="signup-password-error" class="error" for="signup_password" style="display: none;">This field is required.</label>
                </div>
              </div>
            </div>
            <div class="form-horizontal">
              <div class="control-group form-inline">
                <a class="col-md-4 forget-label" href="#"> Forgot Password?</a>
                <a class="col-md-7 remember-label"> Remember me</a>
                
              </div>
            </div>
            <button type="button"  class="btn btn-default login-btn login">LOGIN</button>
          </form>
            <p class="lst-lbl">
                <a href="/index.php/signup" >Not yet a member? SIGNUP</a>
            </p>
            
        </div>
        <div class="col-md-12" style="align-items: center;">
                <img src="{{ ('/images/image4.png') }}" class="app-icon">
                <label class="follow-span">Follow us</label>
                <img src="{{ ('/images/facebook.png') }}" class="social-icon">
                <img src="{{ ('/images/instagram.png') }}" class="social-icon">
            </div>
      <div class="col-md-12" style="align-items: center;color: #fff;margin-top: 25px;"> Release Web version 2.0</div>
      </div>
      <div class="col-md-6 text-center">
       <img src="{{ ('/images/image3.png') }}">
      </div>
    </div>
        
  </div>
</header>



</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-firestore.js"></script>

<script type="text/javascript">
  $(".login").on("click", function(){
        
        var email = $('.signup_email').val();
        var password = $('.signup_password').val();
        var token = $('meta[name="csrf-token"]').attr('content');

        if(email!="" && password!=""){
          document.getElementById("myForm").submit();
            // $.ajax({
            //   headers: { 'X-CSRF-TOKEN':token },
            //   type: "POST",
            //   url: "{{url('userlogin')}}",
            //   data: { email:email, password:password}, 
            //   success: function( msg ) {
            //     console.log(msg);

            //       if(msg==1){
            //         var text = "Login Successfully";
            //         $('#success-email').show();
            //         $('#success-email').text(text);

            //         location.replace("/index.php/dashboard")
            //       }
            //       else{
            //         var text = "Incorrect Login Details";
            //         $('#error-email').show();
            //         $('#error-email').text(text);
            //       }
                   
            //     }
            // });
        }else {
            if(email==""){ $('.signup_email').addClass('error'); $('#signup-email-error').show();
            }else { $('.signup_email').removeClass('error'); $('#signup-email-error').hide(); }

            if(password==""){ $('.signup_password').addClass('error'); $('#signup-password-error').show();
            }else { $('.signup_password').removeClass('error'); $('#signup-password').hide(); }

            
           
        }
    });
</script>

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyDyhC-L2doy8eKRcqlQEsPO3c2YdsbSf_w",
    authDomain: "shotdoctor80.firebaseapp.com",
    databaseURL: "https://shotdoctor80.firebaseio.com",
    projectId: "shotdoctor80",
    storageBucket: "shotdoctor80.appspot.com",
    messagingSenderId: "623442148061",
    appId: "1:623442148061:web:77b2a10f4d21e14ebfb934",
    measurementId: "G-3FB0FR9PR1"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
   const auth = firebase.auth();
    const db = firebase.firestore();
  //firebase.analytics();
</script>
<script type="text/javascript" src="/js/login.js"> </script>
