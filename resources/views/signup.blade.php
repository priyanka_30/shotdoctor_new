@extends('layouts.app')
<!-- Navigation -->
<link href="{{ ('/css/custom.css') }}" rel="stylesheet">
<style type="text/css">
  .error{
    color: red;
    justify-content: left !important;
    padding-left: 14px;
    font-weight: normal !important;
    font-size: 14px;

}
</style>
<div class="bg-img">
<nav class="navbar navbar-expand-lg fixed-top">
  <div class="container">
    <a class="navbar-brand" href="/"><img src="{{ ('/images/logo.png') }}"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link main-a" href="/">HOME </a>
        </li>
        <li class="nav-item">
          <a class="nav-link main-a" href="#">FEATURES</a>
        </li>
        <li class="nav-item">
          <a class="nav-link main-a" href="#">ABOUT</a>
        </li>
        <li class="nav-item">
          <a class="nav-link main-a" href="#">CONTACT</a>
        </li>
        <li class="nav-item">
          <a class="nav-link main-a" href="#">BLOG</a>
        </li>
        <li class="nav-item">
          <a class="nav-link main-a" href="#">DOWNLOAD</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active-p" href="/index.php/signup">SIGNUP</a>
        </li>
        <li class="nav-item">
          <a class="nav-link main-a" href="/index.php/signin">LOGIN</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<!-- Full Page Image Header with Vertically Centered Content -->
<header class="masthead">
  <div class="container h-100">
    <div class="row h-100 align-items-center">
      <div class="col-md-6 text-center">
        
        <div class="signin-div">
            <p class="login-heading">SIGNUP</p>
            <form method="post" action="{{url('registersignup')}}" id="signup-form"  name="myForm"  novalidate="novalidate" >
              @csrf
              <div class="col-md-12" style="margin-bottom: 5px;">
                <span id="error-email" style="background: red; color: #000; display: none;margin-bottom: 10px;padding: 1px 121px;"></span>
              </div>
            <div class="form-horizontal login-m-d">
              <div class="control-group form-inline">
                <label for="name" class="col-md-3 login-label"> FULL NAME</label>
                <div class="controls col-md-9">
                  <input  type="text" id="fullname" name="fullname" class="login-input fullname" placeholder="First name, Last name">
                  <label id="fullname-error" class="error" for="fullname" style="display: none;">This field is required.</label>
                </div>
              </div>
            </div>
            <div class="form-horizontal login-m-d">
              <div class="control-group form-inline">
                <label for="name" class="col-md-3 login-label"> EMAIL</label>
                <div class="controls col-md-9">
                  <input  type="email" id="signup-email" name="email" class="login-input email" placeholder="name@email.com">
                  <label id="email-error" class="error" for="email" style="display: none;">This field is required.</label>
                </div>
              </div>
            </div>
            <div class="form-horizontal login-m-d">
              <div class="control-group form-inline">
                <label for="name" class="col-md-3 login-label"> PASSWORD</label>
                <div class="controls col-md-9">
                  <input type="password" id="password" name="password" class="login-input password" placeholder="Example@123" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,20}" title="Password should have at least 8 characters up to a maximum of 20 characters, which contains at least 1 uppercase & lowercase & 1 numeric value & 1 special character.">
                  <label id="password-error" class="error" for="password" style="display: none;">This field is required.</label>
                </div>
              </div>
            </div>
            <div class="form-horizontal">
              <div class="control-group form-inline">
                <label for="name" class="col-md-3 login-label"> CONFIRM</label>
                <div class="controls col-md-9">
                  <input  type="password" id="confirmpassword" name="confirmpassword" class="login-input confirmpassword" placeholder="*****" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}" >
                  <label id="confirmpassword-error" class="error" for="confirmpassword" style="display: none;">This field is required.</label>
                </div>
              </div>
            </div>
            <div class="form-horizontal">
              <div class="control-group form-inline">
                <a class="col-md-12 siginaccept-label"> By Clicking Sign Up, you agree to our Terms and that you have read our Data Privacy, including Cookie use.</a>
                
              </div>
            </div>
            <button  type="button" class="btn btn-default login-btn saveregister" >SIGN UP</button>
          </form>
            <p class="lst-lbl">
                <a href="/index.php/signin" >Already a member? LOGIN</a>
            </p>
            
        </div>
        <div class="col-md-12" style="align-items: center;">
                <img src="{{ ('/images/image4.png') }}" class="app-icon">
                <label class="follow-span">Follow us</label>
                <img src="{{ ('/images/facebook.png') }}" class="social-icon">
                <img src="{{ ('/images/instagram.png') }}" class="social-icon">
            </div>
        <div class="col-md-12" style="align-items: center;color: #fff;margin-top: 25px;"> Release Web version 2.0</div>
      </div>
      <div class="col-md-6 text-center">
       <img src="{{ ('/images/image3.png') }}">
      </div>
    </div>
  </div>
</header>



</div>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>


<script src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.js'></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
  jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});
  $(function() {
  $("form[name='myForm']").validate({
    // Specify validation rules
    rules: {
     
      fullname: "required",
      // email: {
      //   required: true,
      //  email: true
      // },
      email : "required",
       password: {
        required: true,
        minlength: 8,
        maxlength: 20
      },
      confirmpassword: {
          equalTo: "#password"
      }         

    },
    // Specify validation error messages
    messages: {
      //firstname: "Please enter your Fullname", 
     // email: "Please enter a valid email address",
    //  password: " Must contain at least one number,one special  and one uppercase and lowercase letter, and at least 8 or more characters",
      confirmpassword: " Enter Confirm Password Same as Password"
    },

    //  submitHandler: function(form) {
    //   form.submit();
    // }
   
  });
});


  $(".saveregister").on("click", function(){
       // alert("test");
        var fullname = $('.fullname').val();
        var email = $('.email').val();
        var password = $('.password').val();
        var confirmpassword = $('.confirmpassword').val();
        var token = $('meta[name="csrf-token"]').attr('content');

        if(fullname!="" && email!="" && password!="" && confirmpassword!="" ){
          if(password==confirmpassword){
            $.ajax({
               headers: { 'X-CSRF-TOKEN':token },
               type: "POST",
               url: "{{url('registersignup')}}",
                data: { fullname:fullname, email:email,password:password}, 
                success: function( msg ) {
                   
                    if(msg==1){
                      var text = "Register Successfully";
                      $('#error-email').show();
                      $('#error-email').text(text);

                      location.replace("/index.php/signin")
                    }
                    else{
                      var text = "User Already Exists";
                      $('#error-email').show();
                      $('#error-email').text(text);
                    }
                }
            });
          }
          else{

          }
        }else {
            if(fullname==""){ $('.fullname').addClass('error'); $('#fullname-error').show();
            }else { $('.fullname').removeClass('error'); $('#fullname-error').hide(); }

            if(email==""){ $('.email').addClass('error'); $('#email-error').show();
            }else { $('.email').removeClass('error'); $('#email-error').hide(); }

            if(password==""){ $('.password').addClass('error'); $('#password-error').show();
            }else { $('.password').removeClass('error'); $('#password-error').hide(); }

            if(confirmpassword==""){ $('.confirmpassword').addClass('error'); $('#confirmpassword-error').show();
            }else { $('.confirmpassword').removeClass('error'); $('#confirmpassword-error').hide(); }
           
        }
    });
</script>


<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-firestore.js"></script>

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyDyhC-L2doy8eKRcqlQEsPO3c2YdsbSf_w",
        authDomain: "shotdoctor80.firebaseapp.com",
        databaseURL: "https://shotdoctor80.firebaseio.com",
        projectId: "shotdoctor80",
        storageBucket: "shotdoctor80.appspot.com",
        messagingSenderId: "623442148061",
        appId: "1:623442148061:web:77b2a10f4d21e14ebfb934",
        measurementId: "G-3FB0FR9PR1"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const auth = firebase.auth();
  const db = firebase.firestore();
  //firebase.analytics();
</script>
<script type="text/javascript" src="/js/auth.js"> </script>
