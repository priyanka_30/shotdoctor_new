@extends('layouts.shotdoctor')
@section('content')
    <div style="margin-bottom: 40px;margin-top: -50px;margin-left: 5px;" class="row">
        <div class="col-lg-12">
            <a class="dashboard-a" href="#">
                Dashboard/Users
            </a>
        </div>
    </div>
<div>
    
    <div>
        <div class="table-responsive">
            <table class=" table table-striped datatable">
                <thead style="background: #F2F2F2;">
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Total Shots</th>
                        <th>OSA</th>
                        <th>Drill</th>
                        <th>Gender</th>
                        <th>Age</th>
                    </tr>
                </thead>
                <tbody>
                        <tr>
                            <td>John Doe</td>
                            <td>john@gmail.com</td>
                            <td>300</td>
                            <td>37.78%</td>
                            <td>6 / 12</td>
                            <td>Male</td>
                            <td>26</td>

                        </tr>
                        <tr>
                            <td>Mary Jane</td>
                            <td>mary23@yahoo.com</td>
                            <td>200</td>
                            <td>85%</td>
                            <td>4 / 21</td>
                            <td>Female</td>
                            <td>51</td>

                        </tr>
                        <tr>
                            <td>John Doe</td>
                            <td>john@gmail.com</td>
                            <td>300</td>
                            <td>37.78%</td>
                            <td>6 / 12</td>
                            <td>Male</td>
                            <td>26</td>

                        </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-firestore.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>


    <script>
      // Your web app's Firebase configuration
      var firebaseConfig = {
        apiKey: "AIzaSyDyhC-L2doy8eKRcqlQEsPO3c2YdsbSf_w",
        authDomain: "shotdoctor80.firebaseapp.com",
        databaseURL: "https://shotdoctor80.firebaseio.com",
        projectId: "shotdoctor80",
        storageBucket: "shotdoctor80.appspot.com",
        messagingSenderId: "623442148061",
        appId: "1:623442148061:web:77b2a10f4d21e14ebfb934",
        measurementId: "G-3FB0FR9PR1"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);

      const auth = firebase.auth();
      const db = firebase.firestore();
      //firebase.analytics();
    </script>
    <!-- <script type="text/javascript" src="/js/auth.js"> </script> -->
    <!-- <script type="text/javascript" src="/js/login.js"> </script> -->
    <script type="text/javascript">

       var a =  window.location.href;
     
      if(a == "http://metrics.shot.doctor/index.php/users"){
    firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    var displayName = user.displayName;
    var email = user.email;
    var uid = user.uid;
    var providerData = user.providerData;
   
    document.getElementById("profile-name").innerHTML = displayName;
    var res = displayName.charAt(0);
    document.getElementById("name-first").innerHTML = res;
    console.log("name",displayName);
    console.log(email);
    
    
  } else {
     location.replace("/index.php/signin");
  }
});
}
</script>
@endsection


