@extends('layouts.shotdoctor')
@section('content')

<div>    
    <div>
        <div style="width:500px; margin-bottom:20px;">
        <div style="float:left;margin-right:10px;margin-bottom:20px;padding-top:5px;">Search:</div>
           <form action="/index.php/detail/{{$id}}/" method="get">
            <div style="float:left;margin-right:5px;margin-bottom:20px;"><input type="search" name="search" value="{{$searchtxt}}" class="form-control" placeholder="Enter your search details" style="border-radius:20px;padding-left:15px; width:250px;float:left;">
          <input type="submit" value="GO" class="btn btn-default login-btn login" style="float: left;width: 90px;margin-top: 0px;margin-left: 10px;">
            </div>
            </form>
        </div>
        <div class="table-responsive">
        <div style="margin-bottom: 10;" ><a href="/index.php/userdata"> < Back</a></div>
            <table class="table table-bordered">
                <tr>
                    <th>Date<a href="/index.php/detail/{{$id}}/{{ $sortone }}"><i class="fa fa-fw fa-sort"></i></a></th>
                    <th>Drill<a href="/index.php/detail/{{$id}}/{{ $sorttwo }}"><i class="fa fa-fw fa-sort"></i></a></th>
                    <th>Level<a href="/index.php/detail/{{$id}}/{{ $sortthree }}"><i class="fa fa-fw fa-sort"></i></a></th>
                    <th>Hand<a href="/index.php/detail/{{$id}}/{{ $sortfour }}"><i class="fa fa-fw fa-sort"></i></a></th>
                    <th>Bad<a href="/index.php/detail/{{$id}}/{{ $sortfive }}"><i class="fa fa-fw fa-sort"></i></a></th>
                    <th>Good<a href="/index.php/detail/{{$id}}/{{ $sortsix }}"><i class="fa fa-fw fa-sort"></i></a></th>
                    <th>Total shots<a href="/index.php/detail/{{$id}}/{{ $sortseven }}"><i class="fa fa-fw fa-sort"></i></a></th>
                    <th>Accuracy</th>
                    
                </tr>
                @if($products->count())
                    @foreach($products as $key => $product)
                        <tr>
                            <td>{{ date('m/d/y',strtotime($product->created_on)) }}</td>
                            <td>{{ $drilltype[$product->drill_type] }}</td>
                            <td>{{ $leveltype[$product->level] }}</td>
                            <td>{{ $handtype[$product->guide_hand_type] }}</td>
                            <td>{{ $product->bad_form_shots }}</td>
                            <td>{{ $product->good_form_shots }}</td>
                            <td>{{ $product->total_shots }}</td>
                        @if($product->total_shots >0)
                            <td>{{ round(($product->good_form_shots/$product->total_shots)*100) }}%</td>
                        @else
                            <td>N/A</td>
                       @endif
                        </tr>
                    @endforeach
                @else
                  <tr>
                      <td colspan="8" align="center">No record found</td>
                      
                  </tr>
                @endif
            </table>
            {!! $products->appends(\Request::except('page'))->render() !!}
        </div>
    </div>
</div>
@endsection

