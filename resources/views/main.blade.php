@extends('layouts.app')
<!-- Navigation -->
<link href="{{ ('/css/custom.css') }}" rel="stylesheet">
<div class="bg-img">


<!-- Full Page Image Header with Vertically Centered Content -->
<header class="masthead">
  <div class="container h-100">
    <div class="row h-100 align-items-center">
      <div class="col-md-6 text-center">
        <div class="col-md-12">
        <h3 class="font-weight-light slide-h3">A PROFESSIONAL BASKETBALL COACH ON YOUR WRIST</h3>
      </div>
      <div class="col-md-12">
        <p class="slide-p">The Smartwatch App that eliminates guide hand<br> interference and poor rythm</p>
      </div>
      <div id="outer">
        <div class="inner-btn-login col-md-4">
          <button type="button" class="btn btn-default main-btn" onclick="window.location.href='/index.php/signin'">LOGIN</button>
        </div>
        <div class="inner-btn-signup col-md-4">
          <button type="button" class="btn btn-default main-btn-signup" onclick="window.location.href='/index.php/signup'">SIGNUP</button>
        </div>
      </div>
      <div class="col-md-12">
                <img src="{{ ('/images/image4.png') }}" class="app-icon">
                <label class="follow-span">Follow us</label>
                <img src="{{ ('/images/facebook.png') }}" class="social-icon">
                <img src="{{ ('/images/instagram.png') }}" class="social-icon">
      </div>
      </div>
      <div class="col-md-6 text-center">
       <img src="{{ ('/images/image3.png') }}">
      </div>
    </div>
  </div>
</header>



</div>
