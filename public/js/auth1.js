// signup
function signup(){
	const signupForm = document.querySelector("#signup-form");

	signupForm.addEventListener('submit' , (e) =>{
		e.preventDefault();

		const email = signupForm['signup-email'].value;
		const password = signupForm['password'].value;
		const fullname = signupForm['fullname'].value;
		const confirmpassword = signupForm['confirmpassword'].value;
		if(email!="" && fullname!="" && password==confirmpassword){
		auth.createUserWithEmailAndPassword(email,password).then(cred =>{

			console.log(cred);
			document.getElementById('error-email').innerHTML = "User Created Successfully";
	        document.getElementById("error-email").style.display = "block";

			user = auth.currentUser;

		}).then(function () {
				user.updateProfile({
				displayName: fullname

			});
			}).then(function () {
				 signupForm.reset();
				 window.location = 'verifyemail';
				
			}).catch(function(error) {
				//console.log(error.message);
				document.getElementById('error-email').innerHTML = error.message;
	        	document.getElementById("error-email").style.display = "block";
			});
		}

	});
}

// logout
function logout(){

	firebase.auth().signOut().then(function() {
	  console.log("user signout");
	  window.location = 'main';
	}).catch(function(error) {
	  
	  	var errorCode = error.code;
		var errorMessage = error.message;

		alert("Error:"+ errorMessage);

	});
}

// forgot password 

function validate_email(obj){
    $(obj).val();
    if($(obj).val()!=""){
        $('.signup_email').removeClass('error'); $('#signup-email-error').hide();
    }

}

function forgot_password(){
	var email = $("#email_field").val();

	if(email !=""){

		auth.sendPasswordResetEmail(email).then(function() {

			document.getElementById('error-email').innerHTML = "Email has been sent to you. Please check and verify ";
            document.getElementById("error-email").style.display = "block";
		 
		}).catch(function(error) {
		  	var errorCode = error.code;
			var errorMessage = error.message;

			document.getElementById('error-email').innerHTML = "Incorrect Email-Id";
            document.getElementById("error-email").style.display = "block";
		});

	}
	else{
		$('.signup_email').addClass('error'); $('#signup-email-error').show();
	}
}

// Verify Email 

function verifyemail(){
	user = auth.currentUser;

	user.sendEmailVerification().then(function() {
		document.getElementById('error-email').innerHTML = "Email has been sent to you. Please check and verify ";
		document.getElementById("error-email").style.display = "block";
		document.getElementById("login-m-d").style.display = "none";
		document.getElementById("btn-verify").style.display = "none";

	}).catch(function(error) {
		var errorMessage = error.message;
		
		document.getElementById('error-email').innerHTML = "Incorrect Email-Id";
		document.getElementById("error-email").style.display = "block";
	});
}



