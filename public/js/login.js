
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    var displayName =' ';
    var displayName = user.displayName;
    var email = user.email;
    var uid = user.uid;
    var providerData = user.providerData;
    
    document.getElementById("profile-name").innerHTML = displayName;
    var res = displayName.charAt(0);
    document.getElementById("name-first").innerHTML = res;
    console.log(displayName);
    console.log(email);
    
    
  } else {
    // No user is signed in.
    
  }
});
function validate_email(obj){
    $(obj).val();
    if($(obj).val()!=""){
        $('.signup_email').removeClass('error'); $('#signup-email-error').hide();
    }

}
function validate_password(obj){
    $(obj).val();
    if($(obj).val()!=""){
        $('.signup_password').removeClass('error'); $('#signup-password-error').hide();
    }

}
function login(){
	
	var userEmail = document.getElementById("email_field").value;
	var userPass = document.getElementById("password_field").value;

	if(userEmail!="" && userPass!=""){
    	firebase.auth().signInWithEmailAndPassword(userEmail, userPass).then(function(){

            document.getElementById('error-email').innerHTML = "Login successfully";
            document.getElementById("error-email").style.display = "block";
            window.location = 'dashboard';
      }).catch(function(error){
            var errorCode = error.code;
            var errorMessage = error.message;

            document.getElementById('error-email').innerHTML = "Incorrect User Details";
            document.getElementById("error-email").style.display = "block";

        });
    }
    else{

        if(userEmail==""){ $('.signup_email').addClass('error'); $('#signup-email-error').show();
        }
        else { 

            $('.signup_email').removeClass('error'); $('#signup-email-error').hide();}

        if(userPass==""){ $('.signup_password').addClass('error'); $('#signup-password-error').show();
        }
        else { $('.signup_password').removeClass('error'); $('#signup-password-error').hide();}
    }
}