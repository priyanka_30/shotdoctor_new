// signup
function signup(){
	const signupForm = document.querySelector("#signup-form");

	signupForm.addEventListener('submit' , (e) =>{
		e.preventDefault();

		const email = signupForm['signup-email'].value;
		const password = signupForm['password'].value;
		const fullname = signupForm['fullname'].value;
		const confirmpassword = signupForm['confirmpassword'].value;
		if(email!="" && fullname!="" && password==confirmpassword){
		auth.createUserWithEmailAndPassword(email,password).then(cred =>{

			console.log(cred);
			document.getElementById('error-email').innerHTML = "User Created Successfully";
	        document.getElementById("error-email").style.display = "block";

			user = auth.currentUser;

		}).then(function () {
				user.updateProfile({
				displayName: fullname

			});
			}).then(function () {
				 signupForm.reset();
				// window.location = 'signin';
				location.replace("/index.php/signin")
				
			}).catch(function(error) {
				//console.log(error.message);
				document.getElementById('error-email').innerHTML = error.message;
	        	document.getElementById("error-email").style.display = "block";
			});
		}

	});
}

// logout
function logout(){

	firebase.auth().signOut().then(function() {
	  console.log("user signout");
	 // window.location = '/';
	  location.replace("/index.php/signin")
	  return false;
	}).catch(function(error) {
	  
	  	var errorCode = error.code;
		var errorMessage = error.message;

		alert("Error:"+ errorMessage);

	});
}





